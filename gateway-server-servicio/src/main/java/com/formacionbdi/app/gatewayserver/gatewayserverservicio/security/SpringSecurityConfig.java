package com.formacionbdi.app.gatewayserver.gatewayserverservicio.security;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.Bean;
import org.springframework.http.HttpMethod;
import org.springframework.security.config.annotation.web.reactive.EnableWebFluxSecurity;
import org.springframework.security.config.web.server.SecurityWebFiltersOrder;
import org.springframework.security.config.web.server.ServerHttpSecurity;
import org.springframework.security.web.server.SecurityWebFilterChain;

@EnableWebFluxSecurity
public class SpringSecurityConfig {

    @Autowired
    JwtAuthFilter jwtAuthFilter;

    @Bean
    public SecurityWebFilterChain config(ServerHttpSecurity serverHttpSecurity){
        return serverHttpSecurity.authorizeExchange()
                .pathMatchers("/api/seguridad/oauth/**").permitAll()
                .pathMatchers(HttpMethod.GET, "/api/productos/listar", "/api/items/listar", "/api/usuarios/usuario").permitAll()
                .pathMatchers(HttpMethod.GET, "/api/productos/ver/{id}", "/api/items/ver/{id}/cantidad/{cant}", "/api/usuarios/usuario/{id}").hasAnyRole("ADMIN", "USER")
                .pathMatchers(HttpMethod.POST, "/api/productos/registrar", "/api/items/crear", "/api/usuarios/usuario").hasRole("ADMIN")
                .pathMatchers(HttpMethod.PUT, "/api/productos/editar/{id}", "/api/items/editar/{id}", "/api/usuarios/usuario/{id}").hasRole("ADMIN")
                .pathMatchers(HttpMethod.DELETE, "/api/productos/eliminar/{id}", "/api/items/eliminar/{id}", "/api/usuarios/usuario/{id}").hasRole("ADMIN")
                .anyExchange().authenticated()//cualquier otra ruta no especificada requiere authenticacion
                .and()
                .addFilterAt(jwtAuthFilter, SecurityWebFiltersOrder.AUTHENTICATION)
                .csrf().disable()
                .build();
    }

}

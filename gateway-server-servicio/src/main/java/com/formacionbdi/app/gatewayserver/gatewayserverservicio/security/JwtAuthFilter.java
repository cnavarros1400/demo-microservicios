package com.formacionbdi.app.gatewayserver.gatewayserverservicio.security;

import org.apache.http.HttpHeaders;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.authentication.ReactiveAuthenticationManager;
import org.springframework.security.authentication.UsernamePasswordAuthenticationToken;
import org.springframework.security.core.context.ReactiveSecurityContextHolder;
import org.springframework.stereotype.Component;
import org.springframework.web.server.ServerWebExchange;
import org.springframework.web.server.WebFilter;
import org.springframework.web.server.WebFilterChain;
import reactor.core.publisher.Mono;

@Component
public class JwtAuthFilter implements WebFilter {

    @Autowired
    private ReactiveAuthenticationManager reactiveAuthenticationManager;
    @Override
    public Mono<Void> filter(ServerWebExchange serverWebExchange, WebFilterChain webFilterChain) {
        return Mono.justOrEmpty(serverWebExchange.getRequest().getHeaders().getFirst(HttpHeaders.AUTHORIZATION))
                .filter(authHeader -> authHeader.startsWith("Bearer "))
                .switchIfEmpty(webFilterChain.filter(serverWebExchange).then(Mono.empty()))//abortamos el flujo inicial y devolvemos flujo vacio si no es valido el token
                .map(token -> token.replace("Bearer ", ""))
                .flatMap(token -> reactiveAuthenticationManager.authenticate(new UsernamePasswordAuthenticationToken(null, token)))
                .flatMap(authentication -> webFilterChain.filter(serverWebExchange).contextWrite(ReactiveSecurityContextHolder.withAuthentication(authentication)));
    }
}

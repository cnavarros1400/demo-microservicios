package com.formacionbdi.app.item.controller;

import com.formacionbdi.app.item.model.Item;
import com.formacionbdi.app.item.service.ItemService;
//import com.netflix.hystrix.contrib.javanica.annotation.HystrixCommand;
import io.github.resilience4j.circuitbreaker.annotation.CircuitBreaker;
import io.github.resilience4j.timelimiter.annotation.TimeLimiter;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.cloud.client.circuitbreaker.CircuitBreakerFactory;
import org.springframework.cloud.context.config.annotation.RefreshScope;
import org.springframework.core.env.Environment;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import com.formacionbdi.app.commons.serviciocommons.commons.entity.Producto;

import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.concurrent.CompletableFuture;


@RefreshScope
@RestController
public class ItemController {

    private final Logger log = LoggerFactory.getLogger(ItemController.class);

    @Autowired
    private Environment environment;

    @Autowired
    private CircuitBreakerFactory circuitBreakerFactory;

    @Autowired
    @Qualifier("serviceFeignClient")
    private ItemService itemService;

    @Value("${configuracion.texto}")
    private String texto;

    @GetMapping("/listar")
    public List<Item> listado(@RequestParam(name = "nombre", required = false) String nombre, @RequestHeader(name = "token-request", required = false) String tokenReq){
        System.out.println(nombre);
        System.out.println(tokenReq);
        return itemService.findAll();
    }

    //@HystrixCommand(fallbackMethod="metodoAlternativo")
    @GetMapping("/ver/{id}/cantidad/{cant}")
    public Item detalle(@PathVariable  Long id, @PathVariable Integer cant){
        //return itemService.findById(id, cant); usando hystrix
        return circuitBreakerFactory.create("items").run(() -> itemService.findById(id, cant), e -> metodoAlternativo(id, cant, e));
    }

    @CircuitBreaker(name = "items", fallbackMethod = "metodoAlternativo")
    @GetMapping("/verConAnotaciones/{id}/cantidad/{cant}")
    public Item detalleConAnotaciones(@PathVariable  Long id, @PathVariable Integer cant){
        //return itemService.findById(id, cant); usando hystrix
        return itemService.findById(id, cant);
    }

    @TimeLimiter(name = "items", fallbackMethod = "metodoAlternativoTimeout")
    @GetMapping("/verConAnotacionesTimeout/{id}/cantidad/{cant}")
    public CompletableFuture<Item> detalleConAnotacionesTimeout(@PathVariable  Long id, @PathVariable Integer cant){
        //return itemService.findById(id, cant); usando hystrix
        return CompletableFuture.supplyAsync(() -> itemService.findById(id, cant));
    }

    @CircuitBreaker(name = "items", fallbackMethod = "metodoAlternativo")
    @TimeLimiter(name = "items")//quitar fallbackmethod para que funcionen ambas anotaciones
    @GetMapping("/verCircuitbreakerTimeout/{id}/cantidad/{cant}")
    public CompletableFuture<Item> detalleCircuitbreakerTimeout(@PathVariable  Long id, @PathVariable Integer cant){
        //return itemService.findById(id, cant); usando hystrix
        return CompletableFuture.supplyAsync(() -> itemService.findById(id, cant));
    }

    public Item metodoAlternativo(Long id, Integer cant, Throwable e){
        System.out.println("***INICADA RUTA ALTERNATIVA***");
        log.info(e.getMessage());
        Item item = new Item();
        Producto producto = new Producto();
        item.setCantidad(cant);
        producto.setIdProducto(id);
        producto.setNombre("Panasonic Pantalla");
        producto.setPrecio(30000.00);
        item.setProducto(producto);
        return item;
    }

    public CompletableFuture<Item> metodoAlternativoTimeout(Long id, Integer cant, Throwable e){
        System.out.println("***INICADA RUTA ALTERNATIVA TIMEOUT***");
        log.info(e.getMessage());
        Item item = new Item();
        Producto producto = new Producto();
        item.setCantidad(cant);
        producto.setIdProducto(id);
        producto.setNombre("Panasonic Pantalla");
        producto.setPrecio(30000.00);
        item.setProducto(producto);
        return CompletableFuture.supplyAsync(() -> itemService.findById(id, cant));
    }

    @GetMapping("/leerConfig")
    public ResponseEntity<?> leerConfigServer(@Value("${server.port}") String puerto){
        Map<String, String> json = new HashMap<>();
        json.put("texto", texto);
        json.put("puerto", puerto);
        if (environment.getActiveProfiles().length > 0 && environment.getActiveProfiles()[0].equals("dev")){
            json.put("nombre", environment.getProperty("configuracion.autor.nombre"));
            json.put("correo", environment.getProperty("configuracion.autor.correo"));
        }
        log.info("***Atributos del server config***");
        log.info("***" + texto + ", " + puerto + "***");
        return new ResponseEntity<Map<String, String>>(json, HttpStatus.OK);
    }

    @PostMapping("/crear")
    @ResponseStatus(HttpStatus.CREATED)
    public Producto crear(@RequestBody Producto producto){
        return itemService.save(producto);
    }

    @PutMapping("/editar/{id}")
    @ResponseStatus(HttpStatus.CREATED)
    public Producto editar(@RequestBody Producto producto, @PathVariable Long id){
        return itemService.update(producto, id);
    }

    @DeleteMapping("/eliminar/{id}")
    @ResponseStatus(HttpStatus.OK)
    public void eliminar(@PathVariable Long id){
        itemService.delete(id);
    }

}

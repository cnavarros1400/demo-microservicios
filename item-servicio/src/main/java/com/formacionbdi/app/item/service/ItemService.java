package com.formacionbdi.app.item.service;

import com.formacionbdi.app.item.model.Item;
import com.formacionbdi.app.commons.serviciocommons.commons.entity.Producto;

import java.util.List;

public interface ItemService {
    public List<Item> findAll();
    public Item findById(Long id, Integer cantidad);
    public Producto save(Producto producto);
    public Producto update(Producto producto, Long id);
    public void delete(Long id);
}

package com.formacionbdi.app.item.clientes;

import org.springframework.cloud.openfeign.FeignClient;
import org.springframework.web.bind.annotation.*;
import com.formacionbdi.app.commons.serviciocommons.commons.entity.Producto;

import java.util.List;

@FeignClient(name = "servicio-producto")
public interface ProductoClienteRest {

    @GetMapping("/listar")
    public List<Producto> listadoProductos();

    @GetMapping("/ver/{id}")
    public Producto detalleProducto(@PathVariable Long id);

    @PostMapping("/registrar")
    public Producto crearProducto(@RequestBody Producto producto);

    @PutMapping("/editar/{id}")
    public Producto editarProducto(@RequestBody Producto producto, @PathVariable Long id);

    @DeleteMapping("/eliminar/{id}")
    public void eliminarProducto(@PathVariable Long id);

}

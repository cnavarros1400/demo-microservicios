package com.formacionbdi.app.item;

import io.github.resilience4j.circuitbreaker.CircuitBreakerConfig;
import io.github.resilience4j.timelimiter.TimeLimiterConfig;
import org.springframework.cloud.circuitbreaker.resilience4j.Resilience4JCircuitBreakerFactory;
import org.springframework.cloud.circuitbreaker.resilience4j.Resilience4JConfigBuilder;
import org.springframework.cloud.client.circuitbreaker.Customizer;
import org.springframework.cloud.client.loadbalancer.LoadBalanced;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.web.client.RestTemplate;

import java.time.Duration;

@Configuration
public class AppConfig {


    @Bean("clienteRest")//Si se omite el string, el bean toma el nombre del metodo si solo hay uno
    @LoadBalanced
    public RestTemplate restTemplate(){
        return new RestTemplate();
    }

    @Bean
    public Customizer<Resilience4JCircuitBreakerFactory> defaultCsotmizer(){
        return factory -> factory.configureDefault(id -> {
            return new Resilience4JConfigBuilder(id)
                    .circuitBreakerConfig(CircuitBreakerConfig.custom()
                            .slidingWindowSize(10)//cantidad de peticiones muestra
                            .failureRateThreshold(50)//porcentaje del umbral de falla del microservicio
                            .waitDurationInOpenState(Duration.ofSeconds(10L))//duracion en estado semiabierto
                            .permittedNumberOfCallsInHalfOpenState(5)//por defecto 10
                            .slowCallRateThreshold(50)//umbral de llamadas lentas
                            .slowCallDurationThreshold(Duration.ofSeconds(2L))//timeout
                            .build())
                    //.timeLimiterConfig(TimeLimiterConfig.ofDefaults())
                    .timeLimiterConfig(TimeLimiterConfig.custom().timeoutDuration(Duration.ofSeconds(6L)).build())
                    .build();
        });
    }

}

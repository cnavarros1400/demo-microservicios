package com.formacionbdi.app.item.service;

import com.formacionbdi.app.item.clientes.ProductoClienteRest;
import com.formacionbdi.app.item.model.Item;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.Primary;
import org.springframework.stereotype.Service;

import com.formacionbdi.app.commons.serviciocommons.commons.entity.Producto;

import java.util.List;
import java.util.stream.Collectors;

@Service("serviceFeignClient")
@Primary
public class ItemServiceFeign implements ItemService{

    @Autowired
    private ProductoClienteRest clienteFeign;

    @Override
    public List<Item> findAll() {
        System.out.println("Listado implementado con FeignClient");
        return clienteFeign.listadoProductos().stream().map(p -> new Item(p, 1)).collect(Collectors.toList());
    }

    @Override
    public Item findById(Long id, Integer cantidad) {
        System.out.println("Detalle de producto implementado con FeignClient");
        return new Item(clienteFeign.detalleProducto(id), cantidad);
    }

    @Override
    public Producto save(Producto producto) {
        return clienteFeign.crearProducto(producto);
    }

    @Override
    public Producto update(Producto producto, Long id) {
        return clienteFeign.editarProducto(producto, id);
    }

    @Override
    public void delete(Long id) {
        clienteFeign.eliminarProducto(id);
    }
}

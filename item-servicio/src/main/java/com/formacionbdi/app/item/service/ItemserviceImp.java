package com.formacionbdi.app.item.service;

import com.formacionbdi.app.item.model.Item;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpEntity;
import org.springframework.http.HttpMethod;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Service;
import org.springframework.web.client.RestTemplate;

import com.formacionbdi.app.commons.serviciocommons.commons.entity.Producto;

import java.util.*;
import java.util.stream.Collectors;

@Service("serviceRestTemplate")
public class ItemserviceImp implements ItemService{

    @Autowired
    private RestTemplate restTemplate;

    @Override
    public List<Item> findAll() {
        System.out.println("Listado implementado con RestTemplate");
        List<Producto> productos = Arrays.asList(Objects.requireNonNull(restTemplate.getForObject("http://servicio-producto/listar", Producto[].class)));
        return productos.stream().map(p -> new Item(p, 1)).collect(Collectors.toList());
    }

    @Override
    public Item findById(Long id, Integer cantidad) {
        System.out.println("Detalle de producto implementado con RestTemplate");
        Map<String, String> pathVariables = new HashMap<String, String>();
        pathVariables.put("id", id.toString());
        Producto producto = restTemplate.getForObject("http://servicio-producto/ver/{id}", Producto.class, pathVariables);
        return new Item(producto, cantidad);
    }

    @Override
    public Producto save(Producto producto) {
        HttpEntity<Producto> body = new HttpEntity<Producto>(producto);
        ResponseEntity<Producto> response = restTemplate.exchange("http://servicio-producto/registrar", HttpMethod.POST,body , Producto.class);
        Producto productoResponse = response.getBody();
        return productoResponse;
    }

    @Override
    public Producto update(Producto producto, Long id) {
        Map<String, String> variables = new HashMap<String, String>();
        variables.put("id", id.toString());
        HttpEntity<Producto> body = new HttpEntity<Producto>(producto);
        ResponseEntity<Producto> response = restTemplate.exchange("http://servicio-producto/editar/{id}", HttpMethod.PUT,body,  Producto.class, variables);
        return response.getBody();
    }

    @Override
    public void delete(Long id) {
        Map<String, String> variables = new HashMap<String, String>();
        variables.put("id", id.toString());
        restTemplate.delete("http://servicio-producto/eliminar/{id}", variables);
    }


}

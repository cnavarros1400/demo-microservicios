package com.formacion.bdi.comunes.usuario.comunesusuarios.models.entity;

import javax.persistence.*;
import java.io.Serializable;

@Entity
@Table(name = "rol")
public class Rol implements Serializable {

    private static final long serialVersionUID = -1363303887563433817L;

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private Long idRol;

    @Column(unique = true)
    private String nombreRol;
//    para bidireccionalidad entre usuario y rol
//    @ManyToMany(mappedBy = "roles")
//    private List<Usuario> usuarios;

    public Long getIdRol() {
        return idRol;
    }

    public void setIdRol(Long idRol) {
        this.idRol = idRol;
    }

    public String getNombreRol() {
        return nombreRol;
    }

    public void setNombreRol(String nombreRol) {
        this.nombreRol = nombreRol;
    }


}

package com.formacion.bdi.comunes.usuario.comunesusuarios;

//import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.boot.autoconfigure.jdbc.DataSourceAutoConfiguration;

@SpringBootApplication(exclude = {DataSourceAutoConfiguration.class})
public class CommonsUsuariosApplication {

    /*public static void main(String[] args) {
        SpringApplication.run(CommonsUsuariosApplication.class, args);
    }*/

}

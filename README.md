Proyecto demo basado en microservicios

-Comunicacion entre servicios mediante Feign
-Seguridad Spring Oauth2 JWT/JJWT
-Implementacion de Zuul/Spring Cloud GAteway
-Balanceo de carga Ribbon
-Tolerancia a fallos Hystrix/Resilience4j
-Trazabilidad distribuida: Sleuth y Zipkin
-Implementacion cola demensajeria RabbitMQ
-Implementacion de Servidor de configuracion y servidor de nombres Eureka
-Dockerizacion de microservicios

package com.formacionbdi.app.config.configserverservicio;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.cloud.config.server.EnableConfigServer;

@EnableConfigServer
@SpringBootApplication
public class ConfigServerServicioApplication {

	public static void main(String[] args) {
		SpringApplication.run(ConfigServerServicioApplication.class, args);
	}

}

package com.formacionbdi.app.commons.serviciocommons.commons.entity;

import javax.persistence.*;
import java.io.Serializable;
import java.util.Date;

@Entity
public class Producto implements Serializable {
    private static final long seriaVersionUID = 1L;
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private Long idProducto;
    private String nombre;
    private Double precio;
    @Temporal(TemporalType.DATE)
    private Date fecha_registro;

    @Transient
    private Integer deployedInPort;

    public Date getFecha_registro() {
        return fecha_registro;
    }

    public void setFecha_registro(Date fecha_registro) {
        this.fecha_registro = fecha_registro;
    }

    public Integer getDeployedInPort() {
        return deployedInPort;
    }

    public void setDeployedInPort(Integer deployedInPort) {
        this.deployedInPort = deployedInPort;
    }

    public Long getIdProducto() {
        return idProducto;
    }

    public void setIdProducto(Long idProducto) {
        this.idProducto = idProducto;
    }

    public String getNombre() {
        return nombre;
    }

    public void setNombre(String nombre) {
        this.nombre = nombre;
    }

    public Double getPrecio() {
        return precio;
    }

    public void setPrecio(Double precio) {
        this.precio = precio;
    }

    public Date getFechaRegistro() {
        return fecha_registro;
    }

    public void setFechaRegistro(Date fechaRegistro) {
        this.fecha_registro = fechaRegistro;
    }
}

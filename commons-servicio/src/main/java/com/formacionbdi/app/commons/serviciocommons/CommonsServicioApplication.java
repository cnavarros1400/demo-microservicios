package com.formacionbdi.app.commons.serviciocommons;

//import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.boot.autoconfigure.jdbc.DataSourceAutoConfiguration;

@SpringBootApplication(exclude = {DataSourceAutoConfiguration.class})
public class CommonsServicioApplication {

//    public static void main(String[] args) {
//        SpringApplication.run(CommonsServicioApplication.class, args);
//    }

}

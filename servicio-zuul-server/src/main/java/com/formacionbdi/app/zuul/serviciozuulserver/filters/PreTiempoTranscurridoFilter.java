package com.formacionbdi.app.zuul.serviciozuulserver.filters;

import com.netflix.zuul.ZuulFilter;
import com.netflix.zuul.context.RequestContext;
import com.netflix.zuul.exception.ZuulException;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.stereotype.Component;
import javax.servlet.http.HttpServletRequest;

@Component
public class PreTiempoTranscurridoFilter extends ZuulFilter {

    private static Logger log = LoggerFactory.getLogger(PreTiempoTranscurridoFilter.class);

    @Override
    public String filterType() {
        return "pre";//nombre del filtro es kwyword
    }

    @Override
    public int filterOrder() {
        return 1;
    }

    @Override
    public boolean shouldFilter() {//valida si se debe ejecutar o no el filtro en cada request
        return true;
    }

    @Override
    public Object run() throws ZuulException {//resuelve la logica del filtro
        RequestContext ctx = RequestContext.getCurrentContext();
        HttpServletRequest req = ctx.getRequest();
        Long tiempoIni = System.currentTimeMillis();
        req.setAttribute("tiempoInicio", tiempoIni);
        log.info(String.format("%s request enrutada a %s", req.getMethod(), req.getRequestURL().toString()));
        return null;
    }
}

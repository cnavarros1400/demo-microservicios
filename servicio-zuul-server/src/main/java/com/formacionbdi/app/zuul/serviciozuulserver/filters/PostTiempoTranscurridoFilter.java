package com.formacionbdi.app.zuul.serviciozuulserver.filters;

import com.netflix.zuul.ZuulFilter;
import com.netflix.zuul.context.RequestContext;
import com.netflix.zuul.exception.ZuulException;
import org.slf4j.LoggerFactory;
import org.springframework.stereotype.Component;

import javax.servlet.http.HttpServletRequest;
import org.slf4j.Logger;

@Component
public class PostTiempoTranscurridoFilter extends ZuulFilter {

    private static Logger log = LoggerFactory.getLogger(PostTiempoTranscurridoFilter.class);

    @Override
    public String filterType() {
        return "post";//nombre del filtro es kwyword
    }

    @Override
    public int filterOrder() {
        return 1;
    }

    @Override
    public boolean shouldFilter() {//valida si se debe ejecutar o no el filtro en cada request
        return true;
    }

    @Override
    public Object run() throws ZuulException {//resuelve la logica del filtro
        RequestContext ctx = RequestContext.getCurrentContext();
        HttpServletRequest req = ctx.getRequest();
        Long tiempoIni = (Long) req.getAttribute("tiempoInicio");
        Long tiempoFin = System.currentTimeMillis();
        Long tiempoTranscurrido = tiempoFin - tiempoIni;

        log.info(String.format("Tiempo transcurrido: %s",tiempoTranscurrido.doubleValue()/1000.00));
        return null;
    }
}

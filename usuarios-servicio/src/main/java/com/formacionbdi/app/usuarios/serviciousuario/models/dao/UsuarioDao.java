package com.formacionbdi.app.usuarios.serviciousuario.models.dao;

import com.formacion.bdi.comunes.usuario.comunesusuarios.models.entity.Usuario;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.PagingAndSortingRepository;
import org.springframework.data.repository.query.Param;
import org.springframework.data.rest.core.annotation.RepositoryRestResource;
import org.springframework.data.rest.core.annotation.RestResource;

@RepositoryRestResource(path = "usuario")//preconfigurado de un controller
public interface UsuarioDao extends PagingAndSortingRepository<Usuario, Long> {

    //ruta para metodos personalizados: path/search/nombre_metodo_a_consumir
    // ejemplo/search/buscar-usuario?userNombre=admin
    @RestResource(path = "buscar-usuario")
    public Usuario findByNombreUsuario(@Param("username") String username);

//    Consulta personalizada con jpql
//    @Query("select u from Usuario u where u.nombreUsuario = ?1 and u.correo = ?2")/metodo con multiples parametros
    @Query("select u from Usuario u where u.nombreUsuario = ?1")
    public  Usuario obtenerPorNombreUsuario(String nombre);

}

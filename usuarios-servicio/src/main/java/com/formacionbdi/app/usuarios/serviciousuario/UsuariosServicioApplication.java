package com.formacionbdi.app.usuarios.serviciousuario;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.boot.autoconfigure.domain.EntityScan;

@SpringBootApplication
@EntityScan({"com.formacion.bdi.comunes.usuario.comunesusuarios.models.entity"})//registrando libreria externa al proyecto
public class UsuariosServicioApplication {

	public static void main(String[] args) {
		SpringApplication.run(UsuariosServicioApplication.class, args);
	}

}

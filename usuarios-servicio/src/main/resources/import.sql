INSERT INTO usuario (nombre_usuario, contrasena, correo, nombre, apellidos, habilitado) VALUES ('usuario','$2a$10$cgV8YJqy4JvY.mAAp72M4O8U4udl0BB6Teti9/5srtiP6cXQ5ZOc6', 'usu@mail.com', 'nombreusuario', 'apeusu', true);
INSERT INTO usuario (nombre_usuario, contrasena, correo, nombre, apellidos, habilitado) VALUES ('admin','$2a$10$cTJjpCUZd6bvjws0.2FuFOera.49ZdoOaufbCFbkTuDjFIiA2rEfq','admin@mail.com', 'nombreadmin', 'apeadmin', true);

INSERT INTO rol (nombre_rol) VALUES ('ROLE_USER');
INSERT INTO rol (nombre_rol) VALUES ('ROLE_ADMIN');

INSERT INTO usuario_to_role (usuario_id, rol_id) VALUES (1, 1);
INSERT INTO usuario_to_role (usuario_id, rol_id) VALUES (2, 2);
INSERT INTO usuario_to_role (usuario_id, rol_id) VALUES (2, 1);
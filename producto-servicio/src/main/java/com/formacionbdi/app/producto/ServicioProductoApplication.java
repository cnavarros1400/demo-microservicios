package com.formacionbdi.app.producto;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.boot.autoconfigure.domain.EntityScan;
import org.springframework.cloud.netflix.eureka.EnableEurekaClient;

@EnableEurekaClient
@SpringBootApplication
@EntityScan({"com.formacionbdi.app.commons.serviciocommons.commons.entity"})
public class ServicioProductoApplication {

    public static void main(String[] args) {
        SpringApplication.run(ServicioProductoApplication.class, args);
    }

}

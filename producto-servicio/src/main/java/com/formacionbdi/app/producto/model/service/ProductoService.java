package com.formacionbdi.app.producto.model.service;

//import com.formacionbdi.app.producto.model.entity.Producto;
import com.formacionbdi.app.commons.serviciocommons.commons.entity.Producto;

import java.util.List;

public interface ProductoService {
    public List<Producto> findAll();
    public Producto findById(Long id);
    public Producto save(Producto producto);
    public void deleteById(Long id);
}

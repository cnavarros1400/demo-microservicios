package com.formacionbdi.app.producto.controller;

import com.formacionbdi.app.commons.serviciocommons.commons.entity.Producto;
import com.formacionbdi.app.producto.model.service.ProductoService;
import net.bytebuddy.pool.TypePool;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.core.env.Environment;
import org.springframework.http.HttpStatus;
import org.springframework.web.bind.annotation.*;

import java.util.List;
import java.util.Objects;
import java.util.concurrent.TimeUnit;
import java.util.stream.Collectors;

@RestController
public class ProductoController {

    @Value("${server.port}")//inyecta valores con origen en file properties
    private Integer usedPort;

    @Autowired
    private Environment environment;

    @Autowired
    private ProductoService productoService;

    @GetMapping("/listar")

    public List<Producto> listarProductos(){

        return productoService.findAll().stream().map(producto -> {
            producto.setDeployedInPort(Integer.parseInt(Objects.requireNonNull(environment.getProperty("local.server.port"))));
            //producto.setDeployedInPort(usedPort);
            return producto;
        }).collect(Collectors.toList());
    }

    @GetMapping("/ver/{id}")
    public Producto detalle(@PathVariable Long id) throws InterruptedException {
        if(id.equals(10L)){
            throw new IllegalStateException("***Producto no encontrado***");
        }
        if(id.equals(7L)){
            TimeUnit.SECONDS.sleep(5L);
        }
        Producto producto = productoService.findById(id);
        producto.setDeployedInPort(Integer.parseInt(Objects.requireNonNull(environment.getProperty("local.server.port"))));
        //producto.setDeployedInPort(usedPort);
        //simulando falla
//        boolean ok = false;
//        if(!ok){
//            throw new RuntimeException("No se pudo cargar producto");
//        }
        //simulando timeOut
//        try {
//            Thread.sleep(2000L);
//        } catch (InterruptedException e) {
//            e.printStackTrace();
//        }
        return producto;
    }

    @PostMapping("/registrar")
    @ResponseStatus(HttpStatus.CREATED)
    public Producto registrarProducto(@RequestBody Producto producto){
        return productoService.save(producto);
    }

    @PutMapping("/editar/{id}")
    @ResponseStatus(HttpStatus.CREATED)
    public Producto editarProducto(@RequestBody Producto producto, @PathVariable Long id){
        Producto productoDB = productoService.findById(id);
        productoDB.setNombre(producto.getNombre());
        productoDB.setPrecio(producto.getPrecio());
        return productoService.save(productoDB);
    }

    @DeleteMapping("/eliminar/{id}")
    @ResponseStatus(HttpStatus.OK)
    public void eliminarProducto(@PathVariable Long id){
        productoService.deleteById(id);
    }

}

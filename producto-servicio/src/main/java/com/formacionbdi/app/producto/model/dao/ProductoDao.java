package com.formacionbdi.app.producto.model.dao;


import com.formacionbdi.app.commons.serviciocommons.commons.entity.Producto;
import org.springframework.data.repository.CrudRepository;

public interface ProductoDao extends CrudRepository<Producto, Long> {
}

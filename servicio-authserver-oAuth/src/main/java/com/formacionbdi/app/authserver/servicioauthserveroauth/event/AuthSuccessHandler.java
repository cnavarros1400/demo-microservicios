package com.formacionbdi.app.authserver.servicioauthserveroauth.event;

import brave.Tracer;
import com.formacion.bdi.comunes.usuario.comunesusuarios.models.entity.Usuario;
import com.formacionbdi.app.authserver.servicioauthserveroauth.servicios.IntUsuarioService;
import feign.FeignException;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.authentication.AuthenticationEventPublisher;
import org.springframework.security.core.Authentication;
import org.springframework.security.core.AuthenticationException;
import org.springframework.security.core.userdetails.UserDetails;
import org.springframework.security.web.authentication.WebAuthenticationDetails;
import org.springframework.stereotype.Component;

@Component
public class AuthSuccessHandler implements AuthenticationEventPublisher {

    @Autowired
    private IntUsuarioService intUsuarioService;

    @Autowired
    private Tracer tracer;

    private final Logger log = LoggerFactory.getLogger(AuthSuccessHandler.class);
    @Override
    public void publishAuthenticationSuccess(Authentication authentication) {
//        Usuario usuario = intUsuarioService.findByUsername(authentication.getName());
        //if(authentication.getName().equalsIgnoreCase("frontEndApp")){
        if (authentication.getDetails() instanceof WebAuthenticationDetails){
            return;
        }
        UserDetails userDetails = (UserDetails) authentication.getPrincipal();
        log.info("Acceso exitoso: ".concat(userDetails.toString()));
        Usuario usuario = intUsuarioService.findByUsername(authentication.getName());
        if (usuario.getIntentos() != null && usuario.getIntentos() > 0){
            usuario.setIntentos(0);
            intUsuarioService.updateUser(usuario, usuario.getIdUsuario());
        }
    }
    @Override
    public void publishAuthenticationFailure(AuthenticationException exception, Authentication authentication) {
        String mensaje = "Error de acceso: ".concat(exception.getMessage());
        log.info(mensaje);
        try{
            StringBuilder errors = new StringBuilder();
            errors.append(mensaje);
            Usuario usuario = intUsuarioService.findByUsername(authentication.getName());
            if (usuario.getIntentos() == null){
                usuario.setIntentos(0);
            }
            usuario.setIntentos(usuario.getIntentos() + 1);
            log.info(String.format("Cantidad de intentos de acceso: %s", usuario.getIntentos()));
            errors.append(" - Intentos del login: ").append(usuario.getIntentos());
            if (usuario.getIntentos() >= 3){
                String errorMaxIntentos = String.format("Usuario '%s' deshabilitado, alcanzado limite de intentos de aceso: 3", usuario.getNombreUsuario());
                log.error(errorMaxIntentos);
                errors.append(" - ").append(errorMaxIntentos);
                usuario.setHabilitado(false);
            }
            intUsuarioService.updateUser(usuario, usuario.getIdUsuario());
            tracer.currentSpan().tag("err.mensaje", errors.toString());
            //tracer.currentSpan().customizer().tag("err.mensaje", errors.toString());
        }catch(FeignException fe){
            log.error(String.format("El usuario '%s' no existe en el sistema",authentication.getName()));
        }
    }
}

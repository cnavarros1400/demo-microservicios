package com.formacionbdi.app.authserver.servicioauthserveroauth.servicios;

import com.formacion.bdi.comunes.usuario.comunesusuarios.models.entity.Usuario;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestBody;

public interface IntUsuarioService {
        public Usuario findByUsername(String username);
        public Usuario updateUser(Usuario usuario, Long id);

}

package com.formacionbdi.app.authserver.servicioauthserveroauth.clientes;

import com.formacion.bdi.comunes.usuario.comunesusuarios.models.entity.Usuario;
import org.springframework.cloud.openfeign.FeignClient;
import org.springframework.web.bind.annotation.*;

@FeignClient(name = "servicio-usuario")
public interface UsuarioFeignClient {

    @GetMapping("/usuario/search/buscar-usuario")
    public Usuario findByUserName(@RequestParam String username);

    @PutMapping("/usuario/{id}")
    public Usuario updateUser(@RequestBody Usuario usuario, @PathVariable Long id);

}

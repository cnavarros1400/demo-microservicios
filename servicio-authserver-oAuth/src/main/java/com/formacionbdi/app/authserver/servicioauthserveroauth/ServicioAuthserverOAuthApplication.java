package com.formacionbdi.app.authserver.servicioauthserveroauth;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.CommandLineRunner;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.boot.autoconfigure.jdbc.DataSourceAutoConfiguration;
import org.springframework.cloud.netflix.eureka.EnableEurekaClient;
import org.springframework.cloud.openfeign.EnableFeignClients;
import org.springframework.security.crypto.bcrypt.BCryptPasswordEncoder;

@SpringBootApplication(exclude = {DataSourceAutoConfiguration.class})
@EnableEurekaClient
@EnableFeignClients
public class ServicioAuthserverOAuthApplication implements CommandLineRunner {

    @Autowired
    BCryptPasswordEncoder bCryptPasswordEncoder;
    public static void main(String[] args) {
        SpringApplication.run(ServicioAuthserverOAuthApplication.class, args);
    }

    @Override
    public void run(String... args) throws Exception {
        String password = "123456789";
        for(int i = 0; i <= 4; i++){
            String passBcrypt = bCryptPasswordEncoder.encode(password);
            System.out.println("Contraseña " + (i+1) + ": " +  passBcrypt);
        }
    }
}

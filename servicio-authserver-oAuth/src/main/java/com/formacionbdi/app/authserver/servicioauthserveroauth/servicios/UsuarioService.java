package com.formacionbdi.app.authserver.servicioauthserveroauth.servicios;

import brave.Tracer;
import com.formacion.bdi.comunes.usuario.comunesusuarios.models.entity.Usuario;
import com.formacionbdi.app.authserver.servicioauthserveroauth.clientes.UsuarioFeignClient;
import feign.FeignException;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.core.GrantedAuthority;
import org.springframework.security.core.authority.SimpleGrantedAuthority;
import org.springframework.security.core.userdetails.User;
import org.springframework.security.core.userdetails.UserDetails;
import org.springframework.security.core.userdetails.UserDetailsService;
import org.springframework.security.core.userdetails.UsernameNotFoundException;
import org.springframework.stereotype.Service;

import java.util.List;
import java.util.stream.Collectors;

@Service
public class UsuarioService implements IntUsuarioService, UserDetailsService {

    private final Logger log = LoggerFactory.getLogger(UsuarioService.class);

    @Autowired
    private UsuarioFeignClient usuarioFeignClient;

    @Autowired
    private Tracer tracer;

    @Override
    public UserDetails loadUserByUsername(String username) throws UsernameNotFoundException {
        log.info("Username a buscar: ".concat(username));
        try {
            Usuario usuario = usuarioFeignClient.findByUserName(username);
            List<GrantedAuthority> authorities = usuario.getRoles().stream()
                    .map(role -> new SimpleGrantedAuthority(role.getNombreRol()))
                    .peek(authoritie -> log.info("Rol encontrado para " + username + ": " + authoritie.getAuthority()))
                    .collect(Collectors.toList());
            log.info("Usuario autenticado: " + username);
            return new User(usuario.getNombreUsuario(), usuario.getContrasena(), usuario.getHabilitado(), true, true, true, authorities);
        }catch (FeignException fe){
            String error = "Error de autenticacion, no existe el usuario: " + username;
            tracer.currentSpan().tag("err.mensaje", error + " : " + fe.getMessage());
            log.info(error);
            throw new UsernameNotFoundException(error);
        }
    }

    @Override
    public Usuario findByUsername(String username) {
        return usuarioFeignClient.findByUserName(username);
    }

    @Override
    public Usuario updateUser(Usuario usuario, Long id) {
        return usuarioFeignClient.updateUser(usuario, id);
    }
}

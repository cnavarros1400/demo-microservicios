package com.formacionbdi.app.eureka.servicioeruekaserver;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.cloud.netflix.eureka.server.EnableEurekaServer;

@EnableEurekaServer
@SpringBootApplication
public class EruekaServerServicioApplication {

	public static void main(String[] args) {
		SpringApplication.run(EruekaServerServicioApplication.class, args);
	}

}
